/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author dkhom
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue();
	}
	@Override
	public Temperature toCelsius() {
		float f = this.getValue();
		float c = (f - 32) * 5/9;
		Temperature celsius = new Celsius(c);
				return celsius;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	
}
