/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author dkhom
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue();
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		float c = this.getValue();
		float f = (c * 9 / 5) + 32;
		Temperature fahrenheit = new Fahrenheit(f);
		return fahrenheit;
	}
}
